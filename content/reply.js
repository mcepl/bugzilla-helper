/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Bugzilla Helper.
 *
 * The Initial Developer of the Original Code is
 * Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Shawn Wilsher <me@shawnwilsher.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

Components.utils.import("resource://bugzilla-helper/BugzillaAPI.jsm");
Components.utils.import("resource://bugzilla-helper/BugzillaHelper.jsm");

////////////////////////////////////////////////////////////////////////////////
//// Dialog Controller

var ReplyDialog = {
  initialize: function RD_initialize()
  {
    let label = document.getElementById("comment-label");
    label.value = "Add a comment to bug " + window.arguments[0] + ":";
    let comment = document.getElementById("comment");
    let lines = ["(In reply to comment #" + window.arguments[1] +")"];
    window.arguments[2].split("\n").forEach(function(line) {
      lines.push("> " + line);
    });
    comment.value = lines.join("\n") + "\n";
    comment.focus();
  },

  onAccept: function RD_onAccept()
  {
    let bugID = window.arguments[0];
    let [username, password, store] = Helper.getLoginInformation();
    // XXX handle incorrect auth
    let auth = new BugzillaAuth(username, password);
    let comment = document.getElementById("comment").value;
    let isPrivate = document.getElementById("private").checked;

    let activity = new CommentActivity(bugID);
    activity.start(auth, comment, isPrivate, store);
    return true;
  },

  togglePrivate: function RD_togglePrivate()
  {
    let textbox = document.getElementById("comment");
    let checkbox = document.getElementById("private");
    textbox.className = checkbox.checked ? "private" : "";
  },
};

window.addEventListener("DOMContentLoaded", ReplyDialog.initialize, false);
