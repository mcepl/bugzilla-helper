/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 ts=2 sts=2 et filetype=javascript : */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Bugzilla Helper.
 *
 * The Initial Developer of the Original Code is
 * Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Shawn Wilsher <me@shawnwilsher.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

Components.utils.import("resource://bugzilla-helper/BugzillaAPI.jsm");
Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

let EXPORTED_SYMBOLS = [
  "Helper",
  "CommentActivity",
];

////////////////////////////////////////////////////////////////////////////////
//// Constants

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;

/*
// DEBUG constants
const kBugzillaURI = "https://bugzilla-stage-tip.mozilla.org/";
const kServer = new BugzillaServer(
  "https://api-dev.bugzilla.mozilla.org/stage/latest/"
);
*/

const kBugzillaURI = "https://bugzilla.mozilla.org/";
const kServer = new BugzillaServer(
  "https://api-dev.bugzilla.mozilla.org/latest/"
);
const kBugzillaRealm = "bugzilla-helper";

////////////////////////////////////////////////////////////////////////////////
//// Globals

this.__defineGetter__("activityManager", function() {
  delete this.activityManager;
  return this.activityManager = Cc["@mozilla.org/activity-manager;1"].
                                getService(Ci.nsIActivityManager);
});

////////////////////////////////////////////////////////////////////////////////
//// Dialog Controller

const Helper = {
  /**
   * Obtains the login information by either checking the login manager, or
   * prompting the user.
   *
   * @return a triple containing the username, password, and a boolean value
   *         indicating if the authentication should be saved or not.
   */
  getLoginInformation: function H_getLoginInformation()
  {
    let shouldStore = false;
    let [username, password] = getStoredPassword();
    if (!username || !password) {
      shouldStore = true;
      [username, password] = promptForPassword();
    }

    return [username, password, shouldStore];
  },
};

////////////////////////////////////////////////////////////////////////////////
//// CommentActivity

/**
 * Creates and manages the interaction of an activity for a comment to the
 * given bug number.
 *
 * @param aBugNumber
 *        The bug number this comment is for.
 */
function CommentActivity(aBugNumber)
{
  this._bug = aBugNumber;

  // Create our process activity.
  let process = Cc["@mozilla.org/activity-process;1"].
                createInstance(Ci.nsIActivityProcess);
  process.init("Comment to bug " + aBugNumber, null);
  process.retryHandler = this;

  // Add the process.
  activityManager.addActivity(process);
  this._process = process;
}

CommentActivity.prototype = {
  /**
   * Starts the activity.
   *
   * @param aAuth
   *        The BugzillaAuth to use to authenticate.
   * @param aComment
   *        The text of the comment to add to aBugNumber.
   * @param aIsPrivate
   *        Indicates if this comment should be marked as private or not.
   * @param aStoreAuth
   *        Indicates if authentication should be stored or not upon success.
   */
  start: function CA_start(aAuth,
                           aComment,
                           aIsPrivate,
                           aStoreAuth)
  {
    this._auth = aAuth;
    this._context = {
      bug: this._bug,
      auth: aStoreAuth ? aAuth : null,
      comment: aComment,
      isPrivate: aIsPrivate,
      activity: this,
    };
    BugzillaAPI.addComment(kServer, this._bug, aAuth, aComment, aIsPrivate,
                           commentHandler, this._context);

    // Set our state to being in progress.
    this._process.state = Ci.nsIActivityProcess.STATE_INPROGRESS;
  },

  /**
   * To be called when the action is completed.  Updates the UI to indicate
   * that the comment was posted successfully.
   */
  complete: function CA_complete()
  {
    this._process.setProgress("Submitted", 1, 1);
    this._process.state = Ci.nsIActivityProcess.STATE_COMPLETED;

    // We need to break the cycle between this and the process.
    this._process = null;
  },

  /**
   * To be called when an error occurred.  Updates the UI to indicate that an
   * error occurred and the user should retry.
   */
  error: function CA_error()
  {
    this._process.state = Ci.nsIActivityProcess.STATE_WAITINGFORRETRY;

    // We need to break the cycle between this and the process.
    let process = this._process;
    this._process = null;

    // Notify the user that an error occurred as well.
    let wm = Cc["@mozilla.org/appshell/window-mediator;1"].
             getService(Ci.nsIWindowMediator);
    let win = wm.getMostRecentWindow("mail:3pane");
    if (!win)
      return;
    let nb = win.document.getElementById("mail-notification-box");

    let message = "An error occurred posting a comment to bug " + this._bug;
    let self = this;
    let buttons = [
      {
        label: "Retry",
        callback: function() { self.retry(process) },
      },
    ];
    nb.appendNotification(message, "comment" + this._bug, "",
                          nb.PRIORITY_WARNING_MEDIUM, buttons);
  },

  //////////////////////////////////////////////////////////////////////////////
  //// nsIActivityRetryHandler

  retry: function CA_retry(aProcess)
  {
    // Recreate our cycle - it will break again when we complete.
    this._process = aProcess;

    // Retry by starting over.
    let context = this._context;
    this.start(this._auth, context.comment, context.isPrivate, !!context.auth);
  },

  //////////////////////////////////////////////////////////////////////////////
  //// nsISupports

  QueryInterface: XPCOMUtils.generateQI([
    Ci.nsIActivityRetryHandler,
  ]),
};

////////////////////////////////////////////////////////////////////////////////
//// Global Functions

/**
 * Obtains the stored username and password from the login manager.
 *
 * @return a pair consisting of the username and password.
 */
function getStoredPassword()
{
  let lm = Components.classes["@mozilla.org/login-manager;1"]
                     .getService(Components.interfaces.nsILoginManager);
  let logins = lm.findLogins({}, kBugzillaURI, null, kBugzillaRealm);
  if (!logins.length)
    return [null, null];
  return [logins[0].username, logins[0].password];
}

/**
 * Obtains a username and password by prompting the user.
 *
 * @return a pair consisting of the username and password.
 */
function promptForPassword()
{
  let ps = Components.classes["@mozilla.org/network/default-prompt;1"]
                     .getService(Components.interfaces.nsIAuthPrompt);
  let username = { value: null };
  let password = { value: null };
  let result = ps.promptUsernameAndPassword(
    "Bugzilla Authentication",
    "We'll need your bugzilla username and password to carry on.",
    kBugzillaRealm,
    Components.interfaces.nsIAuthPrompt.SAVE_PASSWORD_PERMANENTLY,
    username,
    password
  );
  if (!result)
    return [null, null];

  return [username.value, password.value];
}

/**
 * Stores the specified username and password into the login manager.
 *
 * @param aUsername
 *        The username to store.
 * @param aPassword
 *        The password to store.
 */
function storePassword(aUsername,
                       aPassword)
{
  // Create the login.
  let l = Components.classes["@mozilla.org/login-manager/loginInfo;1"]
                    .createInstance(Components.interfaces.nsILoginInfo);
  l.init(kBugzillaURI, null, kBugzillaRealm, aUsername, aPassword, "", "");

  // And store it.
  let lm = Components.classes["@mozilla.org/login-manager;1"]
                     .getService(Components.interfaces.nsILoginManager);
  lm.addLogin(l);
}

/**
 * Callback that should be passed to the BugzillaAPI.addComment method.
 *
 * @param aResponseCode
 *        The HTTP response code.  HTTP/201 is what is expected upon success.
 * @param aResponse
 *        The body of the response sent to the server.
 * @param aContext
 *        The context given to BugzillaAPI.addComment.
 */
function commentHandler(aResponseCode,
                        aResponse,
                        aContext)
{
  let activity = aContext.activity;
  if (aResponseCode == 201) {
    activity.complete();
    let auth = aContext.auth;
    if (auth)
      storePassword(auth.username, auth.password);
    return;
  }
  activity.error();
  // We had something unexpected.  Tell the user.
  Components.utils.reportError("Got HTTP/" + aResponseCode + "\n" +
                               aResponse + "\n for comment" + aContext.comment);
}
