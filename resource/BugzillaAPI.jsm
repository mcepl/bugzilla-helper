/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim:set sw=2 ts=2 sts=2 et filetype=javascript : */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Bugzilla Helper.
 *
 * The Initial Developer of the Original Code is
 * Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Shawn Wilsher <me@shawnwilsher.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

let EXPORTED_SYMBOLS = [
  "BugzillaAPI",
  "BugzillaServer",
  "BugzillaAuth",
];

////////////////////////////////////////////////////////////////////////////////
//// Constants

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;

const SEC_ERROR_UNKNOWN_ISSUER = 2153390067;

const kOfflineStatusChanged = "network:offline-status-changed";
const kXpcomShutdown = "xpcom-shutdown";

////////////////////////////////////////////////////////////////////////////////
//// Globals

this.__defineGetter__("ioService", function() {
  delete this.ioService;
  return this.ioService = Cc["@mozilla.org/network/io-service;1"].
                          getService(Ci.nsIIOService);
});

////////////////////////////////////////////////////////////////////////////////
//// BugzillaAPI

const BugzillaAPI = {
  /**
   * Adds a comment to the specified bug.
   *
   * @param aServer
   *        The BugzillaServer to connect to.
   * @param aBugNumber
   *        The bug number to add a comment for.
   * @param aAuth
   *        The BugzillaAuth to use to authenticate.
   * @param aComment
   *        The text of the comment to add to aBugNumber.
   * @param aIsPrivate
   *        Indicates if this comment should be marked as private or not.
   * @param aCallback
   *        The function to call on error or completion with the following
   *        arguments:
   *        aResponseCode - the HTTP response code from the request
   *        aResponse     - the body of the HTTP response
   *        aContext      - the context given to the original function call
   * @param aContext [optional]
   *        An object to be passed back to the callback.
   */
  addComment: function BAPI_addComment(aServer,
                                       aBugNumber,
                                       aAuth,
                                       aComment,
                                       aIsPrivate,
                                       aCallback,
                                       aContext)
  {
    if (!(aServer instanceof BugzillaServer)) {
      throw new Components.Exception(
        "Server must be a BugzillaServer object",
        Cr.NS_ERROR_INVALID_ARG,
        Components.stack.caller
      );
    }
    if (!(aAuth instanceof BugzillaAuth)) {
      throw new Components.Exception(
        "Authentication must be a BugzillaAuth object",
        Cr.NS_ERROR_INVALID_ARG,
        Components.stack.caller
      );
    }

    let uri = aServer.uri + "bug/" + aBugNumber + "/comment?" + aAuth.authString;
    let data = JSON.stringify({
      is_private: aIsPrivate ? 1 : 0,
      text: aComment,
    });
    let callback = function(aResponseCode, aResponse) {
      aCallback(aResponseCode, aResponse, aContext);
    };

    performRequest(aServer, uri, callback, data);
  },
};

////////////////////////////////////////////////////////////////////////////////
//// BugzillaServer

/**
 * Constructor for a server object that represents a bugzilla server.
 *
 * @param aURI
 *        The URI of the server.
 */
function BugzillaServer(aURI)
{
  if (!aURI) {
    throw new Components.Exception(
      "Must provide a server",
      Cr.NS_ERROR_INVALID_ARG,
      Components.stack.caller
    );
  }
  this.uri = aURI;
}

////////////////////////////////////////////////////////////////////////////////
//// BugzillaAuth

/**
 * Constructor for an authentication object that stores a username and password.
 *
 * @param aUsername
 *        The username to authenticate with.
 * @param aPassword
 *        The password to authenticate with.
 */
function BugzillaAuth(aUsername,
                      aPassword)
{
  if (!aUsername || !aPassword) {
    throw new Components.Exception(
      "Must provide a username and a password",
      Cr.NS_ERROR_INVALID_ARG,
      Components.stack.caller
    );
  }
  this.username = aUsername;
  this.password = aPassword;
}

BugzillaAuth.prototype = {
  /**
   * @return the authenication to be added to the url.
   */
  get authString()
  {
    return "username=" + this.username + "&password=" + this.password;
  },
};

////////////////////////////////////////////////////////////////////////////////
//// Global Functions

/**
 * Holds all pending outgoing messages.
 * XXX look into storing this in a database for atomicity.
 */
let gOfflineQueue = [];

/**
 * @param aServer
 *        The server to connect to.
 * @param aURI
 *        The URI to connect to on aServer.
 * @param aCallback
 *        The function to call on error or completion with the following
 *        arguments:
 *        aResponseCode - the HTTP response code from the request
 *        aResponse     - the body of the HTTP response
 * @param aData [optional]
 *        The data to send in the request.
 */
function performRequest(aServer,
                        aURI,
                        aCallback,
                        aData)
{
  // If we are in offline mode, save our arguments and try when we go online.
  if (ioService.offline) {
    gOfflineQueue.push(arguments);
    return;
  }

  let channel = ioService.newChannel(aURI, null, null);
  channel.QueryInterface(Ci.nsIHttpChannel);
  channel.QueryInterface(Ci.nsIUploadChannel);
  channel.QueryInterface(Ci.nsIHttpChannel);
  channel.setRequestHeader("Accept", "application/json", false);
  channel.loadFlags |= Ci.nsIRequest.LOAD_BYPASS_CACHE;
  if (aData) {
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].
                    createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = "UTF-8";
    let is = converter.convertToInputStream(aData);
    channel.setUploadStream(is, "application/json", -1);
    channel.requestMethod = "POST";
  }

  let (listener = Cc["@mozilla.org/network/simple-stream-listener;1"].
                  createInstance(Ci.nsISimpleStreamListener)) {
    let pipe = Cc["@mozilla.org/pipe;1"].createInstance(Ci.nsIPipe);
    pipe.init(true, true, 0, 0xffffffff, null);
    listener.init(pipe.outputStream, {
      onStartRequest: function(aRequest, aContext) { },
      onStopRequest: function(aRequest, aContext, aStatusCode) {
        pipe.outputStream.close();
        let is = Cc["@mozilla.org/scriptableinputstream;1"].
                 createInstance(Ci.nsIScriptableInputStream);
        is.init(pipe.inputStream);

        // available will throw if there is no data since we closed the output
        // end of the pipe.
        let len = 0;
        try {
          len = is.available();
        }
        catch (e) {
        }

        if (len)
          aCallback(channel.responseStatus, is.read(len));
        else
          aCallback(0, null);
      },
    });
    channel.asyncOpen(listener, null);
  }
}

////////////////////////////////////////////////////////////////////////////////
//// Initialization

(function() {
  let os = Cc["@mozilla.org/observer-service;1"].
           getService(Ci.nsIObserverService);

  let observer = {
    observe: function(aSubject, aTopic, aData)
    {
      switch (aTopic) {
        case kOfflineStatusChanged: {
          if (aData == "online") {
            // Process our queue of pending events.
            gOfflineQueue.forEach(function(args) {
              performRequest.apply({ }, args);
            });
          }
        }
        case kXpcomShutdown: {
          // XXX prompt user about unsent messages on shutdown (or store for
          // later sending).
          os.removeObserver(observer, kOfflineStatusChanged);
          os.removeObserver(observer, kXpcomShutdown);
        }
      };
    }
  };

  os.addObserver(observer, kOfflineStatusChanged, false);
  os.addObserver(observer, kXpcomShutdown, false);
})();
